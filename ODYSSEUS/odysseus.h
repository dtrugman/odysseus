#ifndef ODYSSEUS_H
#define ODYSSEUS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "config.h"

#include <Windows.h>

__declspec(dllexport) int8_t ODY_SETUP(ody_how_e how);
__declspec(dllexport) int8_t ODY_INIT(void * to);
__declspec(dllexport) void ODY_DESTROY();

__declspec(dllexport) LRESULT CALLBACK ODY_CB_(int nCode, WPARAM wParam, LPARAM lParam);

#ifdef __cplusplus
}
#endif

#endif /* ODYSSEUS_H */