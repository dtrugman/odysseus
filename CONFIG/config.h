#ifndef CONFIG_H
#define CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#ifdef NDEBUG
#define DBG(...)
#else
#define DBG(...) fprintf(stderr, __VA_ARGS__)
#endif

#define STR(x) #x
#define XSTR(x) STR(x)

#define ODY_DLL "odysseus.dll"

#define ODY_SETUP odysseus_setup
#define ODY_INIT odysseus_init
#define ODY_DESTROY odysseus_destroy
#define ODY_CB_ odysseus_cb
#define ODY_CB _odysseus_cb@12

typedef enum
{
	ODY_HOW_FILE = 0,
	ODY_HOW_UDP = 1
}ody_how_e;

typedef const char * ody_to_file;
typedef struct sockaddr_in ody_to_udp;

typedef int8_t(*ODY_SETUP_FP)(ody_how_e how);
typedef int8_t (*ODY_INIT_FP)(void * to);
typedef void (*ODY_DESTROY_FP)();

#ifdef __cplusplus
}
#endif

#endif /* CONFIG_H */